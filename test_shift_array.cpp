#include <stdio.h>
#include <iostream>

int main()
{
	unsigned char b = 0x80;
	std::cout << "The value of b is " << b << std::endl;
	b = b >> 1;
	std::cout << "The value of b is " << b << std::endl;
	printf("%x\n", b);
}
