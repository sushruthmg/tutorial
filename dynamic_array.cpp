#include <iostream>
int f()
{
	int a, b;
	std::cin >> a;
	std::cin >> b;
	return (a + b);
}

int main()
{
	std::cout << "this is a dynamic array allocation" << std::endl;
	int i;
	i = f();
	char array[1] = {'a'};
	std::string s(array, 1);
	std::cout << s << std::endl;
}
