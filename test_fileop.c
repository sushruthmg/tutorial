#include <stdio.h>
#include <iostream>
#include <fstream>
#define PATH "/home/sushruth/Learning/cplusplus/"
int main()
{
/*
	FILE *fp;
	char buffer[] = {'x', 'Y', 'z', '\n'};
	char buffer1[] = {'a', 'b', 'c', '\n'};
	char buffer2[] = {'d', 'e', 'f', '\n'};
	char buffer3[] = {'i', 'j', 'k', '\n'};
	char buffer4[] = {'q', 'w', 'e', '\n'};
	char buffer5[] = {'z', 'x', 'c', '\n'};
	char readbuf[100];
	std::string fileName(PATH);
	fileName = fileName + "test_file";
	fp = fopen(fileName.c_str(), "a");
	fwrite (buffer, sizeof(char), sizeof(buffer), fp);	
	fwrite (buffer1, sizeof(char), sizeof(buffer), fp);	
	fwrite (buffer2, sizeof(char), sizeof(buffer), fp);	
	fwrite (buffer3, sizeof(char), sizeof(buffer), fp);	
	fwrite (buffer4, sizeof(char), sizeof(buffer), fp);	
	fclose(fp);
	fp = fopen(fileName.c_str(), "r+");
	fread(readbuf, sizeof(char), sizeof(buffer), fp);
	printf("%s\n", readbuf);
	fwrite (buffer5, sizeof(char), sizeof(buffer), fp);	
	fread(readbuf, sizeof(char), sizeof(buffer), fp);
	printf("%s\n", readbuf);
	fread(readbuf, sizeof(char), sizeof(buffer), fp);
	printf("%s\n", readbuf);
	fclose(fp);
*/


	//File operation using fstream
	char buffer[] = {'x', 'Y', 'z', '\n'};
	char buffer1[] = {'a', 'b', 'c', '\n'};
	char buffer2[] = {'d', 'e', 'f', '\n'};
	char buffer3[] = {'i', 'j', 'k', '\n'};
	char buffer4[] = {'q', 'w', 'e', '\n'};
	char buffer5[] = {'z', 'x', 'c', '\n'};
	char readbuf[100];
	std::fstream f;
	if(f.is_open())
	{
		std::cout << "The file is not open" << std::endl;
	}
	f.open((std::string(PATH) + "test_file").c_str(), 
		 std::ios_base::in|std::ios_base::out);
	if (f.is_open())
	{
		std::cout << "As expected" << std::endl;
	}
	else
	{
		std::cout << "WTF" << std::endl;
	}
	f.write(buffer, sizeof(buffer));
	f.read(readbuf, sizeof(buffer3));
	std::cout << readbuf << std::endl;
	f.write(buffer1, sizeof(buffer1));
	f.write(buffer2, sizeof(buffer2));
	f.write(buffer3, sizeof(buffer3));
	f.read(readbuf, sizeof(buffer3));
	std::cout << readbuf << std::endl;
	return 0;
}
