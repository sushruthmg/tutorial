#include <iostream>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

typedef struct {
#ifdef __BIG_ENDIAN__
	uint16_t test : 2;
	uint16_t test1 : 14;

#else
	uint16_t test1 : 14;
	uint16_t test : 2;
#endif
} Test;

int main()
{
	Test t;
	std::cout << "The size of the struct is " << sizeof(t) << std::endl;
	uint16_t a = 0xD000;
	uint16_t *p = &a;
	memcpy(&t, p, sizeof(t));
	std::cout << "The value of test is " << t.test << std::endl;
	return 0;
}
