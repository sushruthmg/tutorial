#include <iostream>
#include <fstream>

int main()
{
	std::fstream fs;
	fs.open("sushruth", std::ios_base::out | std::ios_base::binary);
	fs.write("hello world!!", 12);
	fs.close();
	
	fs.open("sushruth", std::ios_base::out |
			    std::ios_base::in |
			    std::ios_base::binary |
			    std::ios_base::ate);
	std::cout << "write pointer at " << fs.tellp() << ":" << fs.tellg() << std::endl;
	fs.seekg(0, std::ios_base::beg);
	std::streampos fsize = fs.tellg();
	fs.seekg(0, std::ios_base::end);
	fsize = (fs.tellg() - fsize);
	fs.seekp(0, std::ios_base::beg);
	fs.write("test", 4);
	std::cout << "Read ptr " << fs.tellg() << "write ptr " << fs.tellp() << std::endl; 
	fs.close();
	std::cout << "The size of file is " << fsize << std::endl;
}
